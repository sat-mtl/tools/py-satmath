# SATMATH

Provides 3D mathematical functions using the power of NumPy.

This library was forked from [pyrr](https://github.com/adamlwgriffiths/Pyrr) to provide a base and tests but is heavily modified in order to support our needs at the Metalab.

## Installation

### Requirements

- Python 2.6+ / 3.0+
- NumPy

## Original Authors (pyrr)

- [Adam Griffiths](https://github.com/adamlwgriffiths/)
- [Chris Bates](https://github.com/chrsbats/)
- [Jakub Stasiak](https://github.com/jstasiak/)
- [Bogdan Teleaga](https://github.com/bogdanteleaga/)
- [Szabolcs Dombi](https://github.com/cprogrammer1994/)
- [Korijn van Golen](https://github.com/Korijn/)

Contributions are welcome.

## Contributing and Development
See `CONTRIBUTING.md` for development instructions.
