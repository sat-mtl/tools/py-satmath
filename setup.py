from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
from Cython.Distutils import build_ext

setup(
    name='SAT Math',
    cmdclass={'build_ext': build_ext},
    # ext_modules = cythonize("satmath/*.py"),
    ext_modules=cythonize([
        Extension(
            "*",
            [
                "satmath/*.py"
            ],
            language='c++'
        )
    ],
    compiler_directives={
        'always_allow_keywords': True,
        'language_level': 3
    }
    )
)
