try:
    import unittest2 as unittest
except:
    import unittest
import numpy as np

from satmath.euler import Euler
from satmath.matrix44 import Matrix44
from satmath.matrix33 import Matrix33
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3
from satmath.vector4 import Vector4

class test_object_matrix44(unittest.TestCase):
    _shape = (4, 4)
    _size = np.multiply.reduce(_shape)

    def test_create(self):
        m = Matrix44()
        self.assertTrue(np.array_equal(m, np.identity(4)))
        self.assertEqual(m.shape, self._shape)

        m = Matrix44(np.arange(self._size))
        self.assertEqual(m.shape, self._shape)

        m = Matrix44([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]])
        self.assertEqual(m.shape, self._shape)

        m = Matrix44(Matrix44())
        self.assertTrue(np.array_equal(m, np.identity(4)))
        self.assertEqual(m.shape, self._shape)

    def test_identity(self):
        m = Matrix44.identity()
        self.assertTrue(np.array_equal(m, np.eye(4)))

    @unittest.skip('Not implemented')
    def test_perspective_projection(self):
        pass

    @unittest.skip('Not implemented')
    def test_perspective_projection_bounds(self):
        pass

    @unittest.skip('Not implemented')
    def test_orthogonal_projection(self):
        pass

    @unittest.skip('Not implemented')
    def test_from_translation(self):
        pass

    def test_create_from_matrix44(self):
        m1 = Matrix33.identity()
        m = Matrix44.from_matrix33(m1)
        self.assertTrue(np.array_equal(m, np.eye(4)))

        m = Matrix44.from_matrix33(m1)
        self.assertTrue(np.array_equal(m, np.eye(4)))

    def test_create_from_scale(self):
        v = Vector3([1, 2, 3])
        m = Matrix44.from_scale(v)
        self.assertTrue(np.array_equal(m, np.diag([1, 2, 3, 1])))

    def test_create_from_euler(self):
        e = Euler([1, 2, 3])
        m = Matrix44.from_euler(e)
        self.assertTrue(np.allclose(m, np.array([
            [0.4119822, 0.0587266, 0.9092974, 0.0],
            [-0.6812427, -0.6428728, 0.3501755, 0.0],
            [0.6051273, -0.7637183, -0.2248451, 0.0],
            [0.0, 0.0, 0.0, 1.0]
        ]).T))

    def test_create_from_quaternion(self):
        q = Quaternion()
        m = Matrix44.from_quaternion(q)
        self.assertTrue(np.array_equal(m, np.eye(4)))
        self.assertTrue(np.array_equal(m.quaternion, q))

        m = Matrix44.from_quaternion(q)
        self.assertTrue(np.array_equal(m, np.eye(4)))

    # def test_create_from_inverse_quaternion(self):
    #     q = Quaternion.from_x_rotation(0.5)
    #     m = Matrix44.from_inverse_of_quaternion(q)
    #     expected = Matrix44.from_quaternion(quaternion.inverse(quaternion.create_from_x_rotation(0.5)))
    #     np.testing.assert_almost_equal(np.array(m), expected, decimal=5)
    #     # self.assertTrue(np.array_equal(m, expected))

    def test_multiply(self):
        m1 = Matrix44(np.arange(self._size))
        m2 = Matrix44(np.arange(self._size)[::-1])
        m = m1 * m2
        self.assertTrue(np.array_equal(m, np.dot(m2, m1)))

        m1 = Matrix44(np.arange(self._size))
        m2 = Matrix33(np.arange(9))
        m = m1.mul_matrix33(m2)
        self.assertTrue(np.array_equal(m, np.dot(Matrix44.from_matrix33(m2), m1)))

    def test_inverse(self):
        m1 = Matrix44.identity() * Matrix44.from_x_rotation(0.5)
        m = m1.inverse
        self.assertTrue(np.array_equal(m, m1.inverse))

    def test_matrix33(self):
        m1 = Matrix44.identity() * Matrix44.from_x_rotation(0.5)
        m = m1.matrix33
        self.assertTrue(np.array_equal(m, Matrix33.from_matrix44(m1)))

    def test_matrix44(self):
        m1 = Matrix44.identity() * Matrix44.from_x_rotation(0.5)
        m = m1.matrix44
        self.assertTrue(m1 is m)

    def test_operators_matrix33(self):
        m1 = Matrix44.identity()
        m2 = Matrix33.from_x_rotation(0.5)

        # add
        self.assertTrue(np.array_equal(m1.add_matrix33(m2), Matrix44.identity() + Matrix44.from_x_rotation(0.5)))

        # subtract
        self.assertTrue(np.array_equal(m1.sub_matrix33(m2), Matrix44.identity() - Matrix44.from_x_rotation(0.5)))

        # multiply
        self.assertTrue(np.array_equal(m1.mul_matrix33(m2), np.dot(Matrix44.from_x_rotation(0.5), Matrix44.identity())))

        # divide
        self.assertRaises(ValueError, lambda: m1 / m2)

    def test_operators_matrix44(self):
        m1 = Matrix44.identity()
        m2 = Matrix44.from_x_rotation(0.5)

        # add
        self.assertTrue(np.array_equal(m1 + m2, Matrix44.identity() + Matrix44.from_x_rotation(0.5)))

        # subtract
        self.assertTrue(np.array_equal(m1 - m2, Matrix44.identity() - Matrix44.from_x_rotation(0.5)))

        # multiply
        self.assertTrue(np.array_equal(m1 * m2, np.dot(Matrix44.from_x_rotation(0.5), Matrix44.identity())))

        # divide
        self.assertRaises(ValueError, lambda: m1 / m2)

        # inverse
        self.assertTrue(np.array_equal(~m2, Matrix44.from_x_rotation(0.5).inverse))

        # ==
        self.assertTrue(Matrix44() == Matrix44())
        self.assertFalse(Matrix44() == Matrix44([1. for n in range(16)]))

        # !=
        self.assertTrue(Matrix44() != Matrix44([1. for n in range(16)]))
        self.assertFalse(Matrix44() != Matrix44())

    def test_operators_quaternion(self):
        m = Matrix44.identity()
        q = Quaternion.from_x_rotation(0.7)

        # add, not implemented
        # self.assertRaises(ValueError, lambda: m + q)

        # subtract, not implemented
        # self.assertRaises(ValueError, lambda: m - q)

        # multiply
        self.assertTrue(np.array_equal(m.mul_quaternion(q), Matrix44.from_quaternion(Quaternion.from_x_rotation(0.7)) * Matrix44.identity()))

        # divide
        self.assertRaises(ValueError, lambda: m / q)

    def test_operators_vector3(self):
        m = Matrix44.identity()
        v = Vector3([1., 1., 1.])

        # add
        self.assertRaises(ValueError, lambda: m + v)

        # subtract
        self.assertRaises(ValueError, lambda: m - v)

        # multiply
        self.assertTrue(np.array_equal(m.mul_vector3(v), np.dot(Matrix44.identity(), [1., 1., 1., 1.])[:3]))
        self.assertTrue(np.allclose(Matrix44.from_x_rotation(np.pi / 2.).mul_vector3([0., 1., 0.]), [0., 0., 1.]))



        # divide
        self.assertRaises(ValueError, lambda: m / v)

    def test_operators_vector4(self):
        m = Matrix44.identity()
        v = Vector4([1, 1, 1, 1])

        # add, not implemented
        # self.assertRaises(ValueError, lambda: m + v)

        # subtract, not implemented
        # self.assertRaises(ValueError, lambda: m - v)

        # multiply
        self.assertTrue(np.array_equal(m.mul_vector4(v), np.dot(Matrix44.identity(), [1., 1., 1., 1.])))
        self.assertTrue(np.allclose(Matrix44.from_x_rotation(np.pi / 2.).mul_vector4([0., 1., 0., 1.]), [0., 0., 1., 1.]))

        # divide
        self.assertRaises(ValueError, lambda: m / v)

    def test_operators_number(self):
        m = Matrix44.identity()
        fv = np.empty((1,), dtype=[('i', np.int16, 1), ('f', np.float32, 1)])
        fv[0] = (2, 2.0)

        # add
        self.assertTrue(np.array_equal(m.add_number(1.0), Matrix44.identity()[:] + 1.0))
        self.assertTrue(np.array_equal(m.add_number(1), Matrix44.identity()[:] + 1.0))
        self.assertTrue(np.array_equal(m.add_number(np.float(1.)), Matrix44.identity()[:] + 1.0))
        self.assertTrue(np.array_equal(m.add_number(fv[0]['f']), Matrix44.identity()[:] + 2.0))
        self.assertTrue(np.array_equal(m.add_number(fv[0]['i']), Matrix44.identity()[:] + 2.0))

        # subtract
        self.assertTrue(np.array_equal(m.sub_number(1.0), Matrix44.identity()[:] - 1.0))
        self.assertTrue(np.array_equal(m.sub_number(1), Matrix44.identity()[:] - 1.0))
        self.assertTrue(np.array_equal(m.sub_number(np.float(1.)), Matrix44.identity()[:] - 1.0))
        self.assertTrue(np.array_equal(m.sub_number(fv[0]['f']), Matrix44.identity()[:] - 2.0))
        self.assertTrue(np.array_equal(m.sub_number(fv[0]['i']), Matrix44.identity()[:] - 2.0))

        # multiply
        self.assertTrue(np.array_equal(m.mul_number(2.0), Matrix44.identity()[:] * 2.0))
        self.assertTrue(np.array_equal(m.mul_number(2), Matrix44.identity()[:] * 2.0))
        self.assertTrue(np.array_equal(m.mul_number(np.float(2.)), Matrix44.identity()[:] * 2.0))
        self.assertTrue(np.array_equal(m.mul_number(fv[0]['f']), Matrix44.identity()[:] * 2.0))
        self.assertTrue(np.array_equal(m.mul_number(fv[0]['i']), Matrix44.identity()[:] * 2.0))

        # divide
        self.assertTrue(np.array_equal(m.div_number(2.0), np.array(Matrix44.identity()[:]) / 2.0))
        self.assertTrue(np.array_equal(m.div_number(2), np.array(Matrix44.identity()[:]) / 2.0))
        self.assertTrue(np.array_equal(m.div_number(np.float(2.)), np.array(Matrix44.identity()[:]) / 2.0))
        self.assertTrue(np.array_equal(m.div_number(fv[0]['f']), np.array(Matrix44.identity()[:]) / 2.0))
        self.assertTrue(np.array_equal(m.div_number(fv[0]['i']), np.array(Matrix44.identity()[:]) / 2.0))

    def test_accessors(self):
        m = Matrix44(np.arange(self._size))
        self.assertTrue(np.array_equal(m.m1, [0, 1, 2, 3]))
        self.assertTrue(np.array_equal(m.m2, [4, 5, 6, 7]))
        self.assertTrue(np.array_equal(m.m3, [8, 9, 10, 11]))
        self.assertTrue(np.array_equal(m.m4, [12, 13, 14, 15]))

        self.assertTrue(np.array_equal(m.r1, [0, 1, 2, 3]))
        self.assertTrue(np.array_equal(m.r2, [4, 5, 6, 7]))
        self.assertTrue(np.array_equal(m.r3, [8, 9, 10, 11]))
        self.assertTrue(np.array_equal(m.r4, [12, 13, 14, 15]))

        self.assertTrue(np.array_equal(m.c1, [0, 4, 8, 12]))
        self.assertTrue(np.array_equal(m.c2, [1, 5, 9, 13]))
        self.assertTrue(np.array_equal(m.c3, [2, 6, 10, 14]))
        self.assertTrue(np.array_equal(m.c4, [3, 7, 11, 15]))

        self.assertEqual(m.m11, 0)
        self.assertEqual(m.m12, 1)
        self.assertEqual(m.m13, 2)
        self.assertEqual(m.m14, 3)
        self.assertEqual(m.m21, 4)
        self.assertEqual(m.m22, 5)
        self.assertEqual(m.m23, 6)
        self.assertEqual(m.m24, 7)
        self.assertEqual(m.m31, 8)
        self.assertEqual(m.m32, 9)
        self.assertEqual(m.m33, 10)
        self.assertEqual(m.m34, 11)
        self.assertEqual(m.m41, 12)
        self.assertEqual(m.m42, 13)
        self.assertEqual(m.m43, 14)
        self.assertEqual(m.m44, 15)

        m.m11 = 1
        self.assertEqual(m.m11, 1)
        self.assertEqual(m[0, 0], 1)
        m.m11 += 1
        self.assertEqual(m.m11, 2)
        self.assertEqual(m[0, 0], 2)

    @unittest.skip("No dtype support for now")
    def test_decompose(self):
        # define expectations for multiple cases
        testsets = [
            (
                Vector3([1, 1, 2]),
                Quaternion.from_y_rotation(np.pi),
                Vector3([10, 0, -5]),
                Matrix44([
                    [-1, 0, 0, 0],
                    [0, 1, 0, 0],
                    [0, 0, -2, 0],
                    [10, 0, -5, 1],
                ])
            ),
            (
                Vector3([-1, 3, .5]),
                Quaternion.from_axis_rotation(Vector3([.75, .75, 0]).normalized, np.pi).normalized,
                Vector3([1, -1, 1]),
                Matrix44([
                    [0, -1, 0, 0],
                    [3, 0, 0, 0],
                    [0, 0, -.5, 0],
                    [1, -1, 1, 1],
                ])
            ),
        ]

        for expected_scale, expected_rotation, expected_translation, expected_model in testsets:
            # compose model matrix using original inputs
            s = Matrix44.from_scale(expected_scale)
            r = Matrix44.from_quaternion(expected_rotation)
            t = Matrix44.from_translation(expected_translation)
            m = t * r * s

            # check that it's the same as the expected matrix
            np.testing.assert_almost_equal(m, expected_model)
            self.assertTrue(m.dtype == expected_model.dtype)
            self.assertTrue(isinstance(m, expected_model.__class__))

            # decompose this matrix and recompose the model matrix from the decomposition
            dt, dr, ds = m.decompose()
            ds = Matrix44.from_scale(ds)
            dr = Matrix44.from_quaternion(dr)
            dt = Matrix44.from_translation(dt)
            dm = dt * dr * ds

            # check that it's the same as the original matrix
            np.testing.assert_almost_equal(m, dm)
            self.assertTrue(m.dtype == dm.dtype)
            self.assertTrue(isinstance(dm, m.__class__))


if __name__ == '__main__':
    unittest.main()
