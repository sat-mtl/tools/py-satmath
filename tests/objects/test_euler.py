try:
    import unittest2 as unittest
except:
    import unittest
import numpy as np

from satmath.matrix33 import Matrix33
from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
from satmath.euler import Euler, EulerOrder
from satmath.vector4 import Vector4


class TestObjectEuler(unittest.TestCase):
    _shape = (3,)
    _size = np.multiply.reduce(_shape)

    def test_create(self):
        e = Euler()
        self.assertTrue(np.array_equal(e, [0., 0., 0.]))
        self.assertEqual(e.shape, self._shape)

        e = Euler([1., 2., 3.])
        self.assertTrue(np.array_equal(e, [1., 2., 3.]))
        self.assertEqual(e.shape, self._shape)

        e = Euler(Euler())
        self.assertTrue(np.array_equal(e, [0., 0., 0.]))
        self.assertEqual(e.shape, self._shape)

    def test_create_order(self):
        self.assertEqual(Euler().order, EulerOrder.XYZ)
        self.assertEqual(Euler(order=EulerOrder.XZY).order, EulerOrder.XZY)
        self.assertEqual(Euler(order=EulerOrder.YZX).order, EulerOrder.YZX)
        self.assertEqual(Euler(order=EulerOrder.YXZ).order, EulerOrder.YXZ)
        self.assertEqual(Euler(order=EulerOrder.ZXY).order, EulerOrder.ZXY)
        self.assertEqual(Euler(order=EulerOrder.ZYX).order, EulerOrder.ZYX)

    def test_identity(self):
        self.assertEqual(Euler([0., 0., 0.]), Euler.identity())

    def test_create_x_rotation(self):
        self.assertEqual(Euler([1., 0., 0.]), Euler.from_x_rotation(1.))

    def test_create_y_rotation(self):
        self.assertEqual(Euler([0., 1., 0.]), Euler.from_y_rotation(1.))

    def test_create_z_rotation(self):
        self.assertEqual(Euler([0., 0., 1.]), Euler.from_z_rotation(1.))

    def test_operators_iadd_euler(self):
        e = Euler([1.0, 2.0, 3.0])
        e += Euler([0.25, 0.5, 1.0])
        self.assertEqual(e, Euler([1.25, 2.5, 4.0]))

    def test_operators_iadd_float(self):
        e = Euler([1.0, 2.0, 3.0])
        e += 1.0
        self.assertEqual(e, Euler([2.0, 3.0, 4.0]))

    def test_operators_isub_euler(self):
        e = Euler([1.0, 2.0, 3.0])
        e -= Euler([0.25, 0.5, 1.0])
        self.assertEqual(e, Euler([0.75, 1.5, 2.0]))

    def test_operators_isub_float(self):
        e = Euler([1.0, 2.0, 3.0])
        e -= 1.0
        self.assertEqual(e, Euler([0.0, 1.0, 2.0]))

    def test_operators_imul_euler(self):
        e = Euler([1.0, 2.0, 3.0])
        e *= Euler([0.25, 0.5, 1.0])
        self.assertEqual(e, Euler([0.25, 1.0, 3.0]))

    def test_operators_imul_float(self):
        e = Euler([1.0, 2.0, 3.0])
        e *= 2.0
        self.assertEqual(e, Euler([2.0, 4.0, 6.0]))

    def test_operators_itruediv_euler(self):
        e = Euler([1.0, 2.0, 3.0])
        e /= Euler([2.0, 4.0, 3.0])
        self.assertEqual(e, Euler([0.5, 0.5, 1.0]))

    def test_operators_itruediv_float(self):
        e = Euler([1.0, 2.0, 3.0])
        e /= 2.0
        self.assertEqual(e, Euler([0.5, 1.0, 1.5]))

    def test_operators_matrix33(self):
        e = Euler()
        m = Matrix33.from_x_rotation(0.5)

        # add
        self.assertRaises(ValueError, lambda: e + m)

        # subtract
        self.assertRaises(ValueError, lambda: e - m)

        # multiply
        self.assertRaises(ValueError, lambda: e - m)

        # divide
        self.assertRaises(ValueError, lambda: e / m)
        self.assertRaises(ValueError, lambda: e // m)

    def test_operators_matrix44(self):
        e = Euler()
        m = Matrix44.from_x_rotation(0.5)

        # add
        self.assertRaises(ValueError, lambda: e + m)

        # subtract
        self.assertRaises(ValueError, lambda: e - m)

        # multiply
        self.assertRaises(ValueError, lambda: e * m)

        # divide
        self.assertRaises(ValueError, lambda: e / m)
        self.assertRaises(ValueError, lambda: e // m)

    def test_operators_quaternion(self):
        e = Euler()
        q = Quaternion.from_x_rotation(0.5)

        # add
        self.assertRaises(ValueError, lambda: e + q)

        # subtract
        self.assertRaises(ValueError, lambda: e - q)

        # multiply
        self.assertRaises(ValueError, lambda: e * q)

        # divide
        self.assertRaises(ValueError, lambda: e / q)
        self.assertRaises(ValueError, lambda: e // q)

    def test_operators_euler(self):
        e1 = Euler()
        e2 = Euler([1., 2., 3.])

        # add
        self.assertTrue(np.array_equal(e1 + e2, [1., 2., 3.]))

        # subtract
        self.assertTrue(np.array_equal(e1 - e2, [-1., -2., -3.]))

        # multiply
        self.assertTrue(np.array_equal(e1 * e2, [0., 0., 0.]))

        # divide
        self.assertTrue(np.array_equal(e1 / e2, [0., 0., 0.]))

        # or
        self.assertTrue(np.array_equal(e1 | e2, e1.dot(e2)))

        # xor
        self.assertTrue(np.array_equal(e1 ^ e2, e1.cross(e2)))

        # ==
        self.assertTrue(Euler() == Euler())
        self.assertFalse(Euler() == Euler([1., 1., 1.]))

        # !=
        self.assertTrue(Euler() != Euler([1., 1., 1.]))
        self.assertFalse(Euler() != Euler())

    def test_operators_vector4(self):
        e1 = Euler()
        e2 = Vector4([1., 2., 3., 4.])

        # add
        self.assertRaises(ValueError, lambda: e1 + e2)

        # subtract
        self.assertRaises(ValueError, lambda: e1 - e2)

        # multiply
        self.assertRaises(ValueError, lambda: e1 * e2)

        # divide
        self.assertRaises(ValueError, lambda: e1 / e2)

        # or
        self.assertRaises(ValueError, lambda: e1 | e2)

        # xor
        # self.assertRaises(ValueError, lambda: e1 ^ e2)

        # ==
        # self.assertRaises(ValueError, lambda: Euler() == Vector4())

        # !=
        # self.assertRaises(ValueError, lambda: Euler() != Vector4([1., 1., 1., 1.]))

    def test_operators_number(self):
        e1 = Euler([1., 2., 3.])
        fv = np.empty((1,), dtype=[('i', np.int16, 1), ('f', np.float32, 1)])
        fv[0] = (2, 2.0)

        # add
        self.assertTrue(np.array_equal(e1 + 1., [2., 3., 4.]))
        self.assertTrue(np.array_equal(e1 + 1, [2., 3., 4.]))
        self.assertTrue(np.array_equal(e1 + np.float(1.), [2., 3., 4.]))
        self.assertTrue(np.array_equal(e1 + fv[0]['f'], [3., 4., 5.]))
        self.assertTrue(np.array_equal(e1 + fv[0]['i'], [3., 4., 5.]))

        # subtract
        self.assertTrue(np.array_equal(e1 - 1., [0., 1., 2.]))
        self.assertTrue(np.array_equal(e1 - 1, [0., 1., 2.]))
        self.assertTrue(np.array_equal(e1 - np.float(1.), [0., 1., 2.]))
        self.assertTrue(np.array_equal(e1 - fv[0]['f'], [-1., 0., 1.]))
        self.assertTrue(np.array_equal(e1 - fv[0]['i'], [-1., 0., 1.]))

        # multiply
        self.assertTrue(np.array_equal(e1 * 2., [2., 4., 6.]))
        self.assertTrue(np.array_equal(e1 * 2, [2., 4., 6.]))
        self.assertTrue(np.array_equal(e1 * np.float(2.), [2., 4., 6.]))
        self.assertTrue(np.array_equal(e1 * fv[0]['f'], [2., 4., 6.]))
        self.assertTrue(np.array_equal(e1 * fv[0]['i'], [2., 4., 6.]))

        # divide
        self.assertTrue(np.array_equal(e1 / 2., [.5, 1., 1.5]))
        self.assertTrue(np.array_equal(e1 / 2, [.5, 1., 1.5]))
        self.assertTrue(np.array_equal(e1 / np.float(2.), [.5, 1., 1.5]))
        self.assertTrue(np.array_equal(e1 / fv[0]['f'], [.5, 1., 1.5]))
        self.assertTrue(np.array_equal(e1 / fv[0]['i'], [.5, 1., 1.5]))

        # or
        # self.assertRaises(ValueError, lambda: e1 | .5)
        # self.assertRaises(ValueError, lambda: e1 | 5)
        # self.assertRaises(ValueError, lambda: e1 | np.float(2.))
        # self.assertRaises(ValueError, lambda: e1 | fv[0]['f'])
        # self.assertRaises(ValueError, lambda: e1 | fv[0]['i'])
        self.assertRaises(TypeError, lambda: e1 | .5)
        self.assertRaises(TypeError, lambda: e1 | 5)
        self.assertRaises(TypeError, lambda: e1 | np.float(2.))
        self.assertRaises(TypeError, lambda: e1 | fv[0]['f'])
        self.assertRaises(TypeError, lambda: e1 | fv[0]['i'])

        # xor
        # self.assertRaises(ValueError, lambda: e1 ^ .5)
        # self.assertRaises(ValueError, lambda: e1 ^ 5)
        # self.assertRaises(ValueError, lambda: e1 ^ np.float(2.))
        # self.assertRaises(ValueError, lambda: e1 ^ fv[0]['f'])
        # self.assertRaises(ValueError, lambda: e1 ^ fv[0]['i'])

        # ==
        # self.assertRaises(ValueError, lambda: e1 == .5)
        # self.assertRaises(ValueError, lambda: e1 == 5)
        # self.assertRaises(ValueError, lambda: e1 == np.float(2.))
        # self.assertRaises(ValueError, lambda: e1 == fv[0]['f'])
        # self.assertRaises(ValueError, lambda: e1 == fv[0]['i'])

        # !=
        # self.assertRaises(ValueError, lambda: e1 != .5)
        # self.assertRaises(ValueError, lambda: e1 != 5)
        # self.assertRaises(ValueError, lambda: e1 != np.float(2.))
        # self.assertRaises(ValueError, lambda: e1 != fv[0]['f'])
        # self.assertRaises(ValueError, lambda: e1 != fv[0]['i'])

    def test_bitwise(self):
        e1 = Euler([1., 0., 0.])
        e2 = Euler([0., 1., 0.])

        # xor (cross)
        self.assertTrue(np.array_equal(e1 ^ e2, e1.cross(e2)))

        # or (dot)
        self.assertTrue(np.array_equal(e1 | e2, e1.dot(e2)))

    def test_accessors(self):
        e = Euler(np.arange(self._size))
        self.assertTrue(np.array_equal(e.xy, [0, 1]))
        self.assertTrue(np.array_equal(e.xyz, [0, 1, 2]))
        self.assertTrue(np.array_equal(e.xz, [0, 2]))
        self.assertTrue(np.array_equal(e.xyz, [0, 1, 2]))

        self.assertEqual(e.x, 0)
        self.assertEqual(e.y, 1)
        self.assertEqual(e.z, 2)

        e.x = 1
        self.assertEqual(e.x, 1)
        self.assertEqual(e[0], 1)
        e.x += 1
        self.assertEqual(e.x, 2)
        self.assertEqual(e[0], 2)

    def test_inverse(self):
        e = Euler([np.pi / 2.0, 0.0, np.pi])
        self.assertTrue(np.allclose(e.inverse, [np.pi / 2.0, 0.0, -np.pi]))

    def test_get_length_squared(self):
        e = Euler([1., 0., 0.])
        self.assertEqual(1.0, e.squared_length)
        e = Euler([1., 2., 3.])
        self.assertAlmostEqual(14., e.squared_length)

    def test_get_length(self):
        e = Euler([1., 0., 0.])
        self.assertEqual(1.0, e.length)
        e = Euler([1., 2., 3.])
        self.assertAlmostEqual(3.74166, e.length, 5)

    def test_set_length(self):
        e = Euler([1., 0., 0.])
        e.length = 2.
        self.assertEqual(2.0, e.length)
        e = Euler([1., 2., 3.])
        e.length = 3.74166 * 2
        self.assertAlmostEqual(3.74166 * 2, e.length, 5)

    def test_normalize(self):
        e = Euler([1., 1., 1.])
        np.testing.assert_almost_equal(e.normalized, [0.57735, 0.57735, 0.57735], decimal=5)
        self.assertEqual(e, Euler([1., 1., 1.]))

        e.normalize()
        np.testing.assert_almost_equal(e, [0.57735, 0.57735, 0.57735], decimal=5)

    def test_interpolate(self):
        a = Euler([1., 2., 3.])
        b = Euler([2., 3., 4.])
        c = a.interpolate(b, 0.5)
        self.assertIsInstance(c, Euler)
        self.assertEqual(Euler([1.5, 2.5, 3.5]), c)


if __name__ == '__main__':
    unittest.main()
