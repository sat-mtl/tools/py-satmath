try:
    import unittest2 as unittest
except:
    import unittest
import numpy as np

from satmath.matrix33 import Matrix33
from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3
from satmath.vector4 import Vector4


class TestObjectVector3(unittest.TestCase):
    _shape = (3,)
    _size = np.multiply.reduce(_shape)

    def test_create(self):
        v = Vector3()
        self.assertTrue(np.array_equal(v, [0., 0., 0.]))
        self.assertEqual(v.shape, self._shape)

        v = Vector3([1., 2., 3.])
        self.assertTrue(np.array_equal(v, [1., 2., 3.]))
        self.assertEqual(v.shape, self._shape)

        v = Vector3(Vector3())
        self.assertTrue(np.array_equal(v, [0., 0., 0.]))
        self.assertEqual(v.shape, self._shape)

        v4 = Vector4([1., 2., 3., 4.])
        result = Vector3.from_vector4(v4)
        v = result
        np.testing.assert_almost_equal(v, [1., 2., 3.], decimal=5)

        v4 = Vector4([1., 2., 3., 4.])
        result = Vector3.from_vector4(v4)
        np.testing.assert_almost_equal(result, [1., 2., 3.], decimal=5)

        m = Matrix44.from_translation([1., 2., 3.])
        v = Vector3.from_matrix44_translation(m)
        self.assertTrue(np.array_equal(v, [1., 2., 3.]))

        m = Matrix44.from_translation([1., 2., 3.])
        v = Vector3.from_matrix44_translation(m)
        self.assertTrue(np.array_equal(v, [1., 2., 3.]))

    def test_create_x_axis(self):
        self.assertEqual(Vector3([1., 0., 0.]), Vector3.x_axis())

    def test_create_y_axis(self):
        self.assertEqual(Vector3([0., 1., 0.]), Vector3.y_axis())

    def test_create_z_axis(self):
        self.assertEqual(Vector3([0., 0., 1.]), Vector3.z_axis())

    def test_operators_iadd_vec3(self):
        v = Vector3([1.0, 2.0, 3.0])
        v += Vector3([0.25, 0.5, 1.0])
        self.assertEqual(v, Vector3([1.25, 2.5, 4.0]))

    def test_operators_iadd_float(self):
        v = Vector3([1.0, 2.0, 3.0])
        v += 1.0
        self.assertEqual(v, Vector3([2.0, 3.0, 4.0]))

    def test_operators_isub_vec3(self):
        v = Vector3([1.0, 2.0, 3.0])
        v -= Vector3([0.25, 0.5, 1.0])
        self.assertEqual(v, Vector3([0.75, 1.5, 2.0]))

    def test_operators_isub_float(self):
        v = Vector3([1.0, 2.0, 3.0])
        v -= 1.0
        self.assertEqual(v, Vector3([0.0, 1.0, 2.0]))

    def test_operators_imul_vec3(self):
        v = Vector3([1.0, 2.0, 3.0])
        v *= Vector3([0.25, 0.5, 1.0])
        self.assertEqual(v, Vector3([0.25, 1.0, 3.0]))

    def test_operators_imul_float(self):
        v = Vector3([1.0, 2.0, 3.0])
        v *= 2.0
        self.assertEqual(v, Vector3([2.0, 4.0, 6.0]))

    def test_operators_itruediv_vec3(self):
        v = Vector3([1.0, 2.0, 3.0])
        v /= Vector3([2.0, 4.0, 3.0])
        self.assertEqual(v, Vector3([0.5, 0.5, 1.0]))

    def test_operators_itruediv_float(self):
        v = Vector3([1.0, 2.0, 3.0])
        v /= 2.0
        self.assertEqual(v, Vector3([0.5, 1.0, 1.5]))

    def test_operators_matrix33(self):
        v = Vector3()
        m = Matrix33.from_x_rotation(0.5)

        # add
        self.assertRaises(ValueError, lambda: v + m)

        # subtract
        self.assertRaises(ValueError, lambda: v - m)

        # multiply
        self.assertRaises(ValueError, lambda: v - m)

        # divide
        self.assertRaises(ValueError, lambda: v / m)
        self.assertRaises(ValueError, lambda: v // m)

    def test_operators_matrix44(self):
        v = Vector3()
        m = Matrix44.from_x_rotation(0.5)

        # add
        self.assertRaises(ValueError, lambda: v + m)

        # subtract
        self.assertRaises(ValueError, lambda: v - m)

        # multiply
        self.assertRaises(ValueError, lambda: v * m)

        # divide
        self.assertRaises(ValueError, lambda: v / m)
        self.assertRaises(ValueError, lambda: v // m)

    def test_operators_quaternion(self):
        v = Vector3()
        q = Quaternion.from_x_rotation(0.5)

        # add
        self.assertRaises(ValueError, lambda: v + q)

        # subtract
        self.assertRaises(ValueError, lambda: v - q)

        # multiply
        self.assertRaises(ValueError, lambda: v * q)

        # divide
        self.assertRaises(ValueError, lambda: v / q)
        self.assertRaises(ValueError, lambda: v // q)

    def test_operators_vector3(self):
        v1 = Vector3()
        v2 = Vector3([1., 2., 3.])

        # add
        self.assertTrue(np.array_equal(v1 + v2, [1., 2., 3.]))

        # subtract
        self.assertTrue(np.array_equal(v1 - v2, [-1., -2., -3.]))

        # multiply
        self.assertTrue(np.array_equal(v1 * v2, [0., 0., 0.]))

        # divide
        self.assertTrue(np.array_equal(v1 / v2, [0., 0., 0.]))

        # or
        self.assertTrue(np.array_equal(v1 | v2, v1.dot(v2)))

        # xor
        self.assertTrue(np.array_equal(v1 ^ v2, v1.cross(v2)))

        # ==
        self.assertTrue(Vector3() == Vector3())
        self.assertFalse(Vector3() == Vector3([1., 1., 1.]))

        # !=
        self.assertTrue(Vector3() != Vector3([1., 1., 1.]))
        self.assertFalse(Vector3() != Vector3())

    def test_operators_vector4(self):
        v1 = Vector3()
        v2 = Vector4([1., 2., 3., 4.])

        # add
        self.assertRaises(ValueError, lambda: v1 + v2)

        # subtract
        self.assertRaises(ValueError, lambda: v1 - v2)

        # multiply
        self.assertRaises(ValueError, lambda: v1 * v2)

        # divide
        self.assertRaises(ValueError, lambda: v1 / v2)

        # or
        self.assertRaises(ValueError, lambda: v1 | v2)

        # xor
        self.assertRaises(ValueError, lambda: v1 ^ v2)

        # ==
        # self.assertRaises(ValueError, lambda: Vector3() == Vector4())

        # !=
        # self.assertRaises(ValueError, lambda: Vector3() != Vector4([1., 1., 1., 1.]))

    def test_operators_number(self):
        v1 = Vector3([1., 2., 3.])
        fv = np.empty((1,), dtype=[('i', np.int16, 1), ('f', np.float32, 1)])
        fv[0] = (2, 2.0)

        # add
        self.assertTrue(np.array_equal(v1 + 1., [2., 3., 4.]))
        self.assertTrue(np.array_equal(v1 + 1, [2., 3., 4.]))
        self.assertTrue(np.array_equal(v1 + np.float(1.), [2., 3., 4.]))
        self.assertTrue(np.array_equal(v1 + fv[0]['f'], [3., 4., 5.]))
        self.assertTrue(np.array_equal(v1 + fv[0]['i'], [3., 4., 5.]))

        # subtract
        self.assertTrue(np.array_equal(v1 - 1., [0., 1., 2.]))
        self.assertTrue(np.array_equal(v1 - 1, [0., 1., 2.]))
        self.assertTrue(np.array_equal(v1 - np.float(1.), [0., 1., 2.]))
        self.assertTrue(np.array_equal(v1 - fv[0]['f'], [-1., 0., 1.]))
        self.assertTrue(np.array_equal(v1 - fv[0]['i'], [-1., 0., 1.]))

        # multiply
        self.assertTrue(np.array_equal(v1 * 2., [2., 4., 6.]))
        self.assertTrue(np.array_equal(v1 * 2, [2., 4., 6.]))
        self.assertTrue(np.array_equal(v1 * np.float(2.), [2., 4., 6.]))
        self.assertTrue(np.array_equal(v1 * fv[0]['f'], [2., 4., 6.]))
        self.assertTrue(np.array_equal(v1 * fv[0]['i'], [2., 4., 6.]))

        # divide
        self.assertTrue(np.array_equal(v1 / 2., [.5, 1., 1.5]))
        self.assertTrue(np.array_equal(v1 / 2, [.5, 1., 1.5]))
        self.assertTrue(np.array_equal(v1 / np.float(2.), [.5, 1., 1.5]))
        self.assertTrue(np.array_equal(v1 / fv[0]['f'], [.5, 1., 1.5]))
        self.assertTrue(np.array_equal(v1 / fv[0]['i'], [.5, 1., 1.5]))

        # or
        # self.assertRaises(ValueError, lambda: v1 | .5)
        # self.assertRaises(ValueError, lambda: v1 | 5)
        # self.assertRaises(ValueError, lambda: v1 | np.float(2.))
        # self.assertRaises(ValueError, lambda: v1 | fv[0]['f'])
        # self.assertRaises(ValueError, lambda: v1 | fv[0]['i'])
        self.assertRaises(TypeError, lambda: v1 | .5)
        self.assertRaises(TypeError, lambda: v1 | 5)
        self.assertRaises(TypeError, lambda: v1 | np.float(2.))
        self.assertRaises(TypeError, lambda: v1 | fv[0]['f'])
        self.assertRaises(TypeError, lambda: v1 | fv[0]['i'])

        # xor
        self.assertRaises(ValueError, lambda: v1 ^ .5)
        self.assertRaises(ValueError, lambda: v1 ^ 5)
        self.assertRaises(ValueError, lambda: v1 ^ np.float(2.))
        self.assertRaises(ValueError, lambda: v1 ^ fv[0]['f'])
        self.assertRaises(ValueError, lambda: v1 ^ fv[0]['i'])

        # ==
        # self.assertRaises(ValueError, lambda: v1 == .5)
        # self.assertRaises(ValueError, lambda: v1 == 5)
        # self.assertRaises(ValueError, lambda: v1 == np.float(2.))
        # self.assertRaises(ValueError, lambda: v1 == fv[0]['f'])
        # self.assertRaises(ValueError, lambda: v1 == fv[0]['i'])

        # !=
        # self.assertRaises(ValueError, lambda: v1 != .5)
        # self.assertRaises(ValueError, lambda: v1 != 5)
        # self.assertRaises(ValueError, lambda: v1 != np.float(2.))
        # self.assertRaises(ValueError, lambda: v1 != fv[0]['f'])
        # self.assertRaises(ValueError, lambda: v1 != fv[0]['i'])

    def test_bitwise(self):
        v1 = Vector3([1., 0., 0.])
        v2 = Vector3([0., 1., 0.])

        # xor (cross)
        self.assertTrue(np.array_equal(v1 ^ v2, v1.cross(v2)))

        # or (dot)
        self.assertTrue(np.array_equal(v1 | v2, v1.dot(v2)))

    def test_accessors(self):
        v = Vector3(np.arange(self._size))
        self.assertTrue(np.array_equal(v.xy, [0, 1]))
        self.assertTrue(np.array_equal(v.xyz, [0, 1, 2]))
        self.assertTrue(np.array_equal(v.xz, [0, 2]))
        self.assertTrue(np.array_equal(v.xyz, [0, 1, 2]))

        self.assertEqual(v.x, 0)
        self.assertEqual(v.y, 1)
        self.assertEqual(v.z, 2)

        v.x = 1
        self.assertEqual(v.x, 1)
        self.assertEqual(v[0], 1)
        v.x += 1
        self.assertEqual(v.x, 2)
        self.assertEqual(v[0], 2)

    def test_vector3(self):
        v = Vector3([1., 2., 3.])
        self.assertIsNot(v, v.vector3)
        self.assertEqual(v, v.vector3)

    def test_vector4(self):
        v = Vector3([1., 2., 3.])
        self.assertEqual(Vector4([1., 2., 3., 0.]), v.vector4)

    def test_inverse(self):
        v = Vector3([1., 2., 3.])
        self.assertTrue(np.array_equal(v.inverse, [-1., -2., -3.]))

    def test_get_length_squared(self):
        v = Vector3([1., 0., 0.])
        self.assertEqual(1.0, v.squared_length)
        v = Vector3([1., 2., 3.])
        self.assertAlmostEqual(14., v.squared_length)

    def test_get_length(self):
        v = Vector3([1., 0., 0.])
        self.assertEqual(1.0, v.length)
        v = Vector3([1., 2., 3.])
        self.assertAlmostEqual(3.74166, v.length, 5)

    def test_set_length(self):
        v = Vector3([1., 0., 0.])
        v.length = 2.
        self.assertEqual(2.0, v.length)
        v = Vector3([1., 2., 3.])
        v.length = 3.74166 * 2
        self.assertAlmostEqual(3.74166 * 2, v.length, 5)

    def test_normalize(self):
        v = Vector3([1., 1., 1.])
        np.testing.assert_almost_equal(v.normalized, [0.57735, 0.57735, 0.57735], decimal=5)
        self.assertEqual(v, Vector3([1., 1., 1.]))

        v.normalize()
        np.testing.assert_almost_equal(v, [0.57735, 0.57735, 0.57735], decimal=5)

    def test_interpolate(self):
        a = Vector3([1., 2., 3.])
        b = Vector3([2., 3., 4.])
        c = a.interpolate(b, 0.5)
        self.assertIsInstance(c, Vector3)
        self.assertEqual(Vector3([1.5, 2.5, 3.5]), c)

    def test_normal(self):
        v1 = Vector3([1.0, 0.0, 0.0])
        v2 = Vector3([0.0, 0.0, 0.0])
        v3 = Vector3([0.0, 1.0, 0.0])
        n = v1.normal(v2, v3)
        self.assertEqual(Vector3([0., 0., -1.]), n)

    def test_normal_normalize(self):
        v1 = Vector3([2.0, 4.0, 2.0])
        v2 = Vector3([1.0, 7.0, 3.0])
        v3 = Vector3([8.0, 1.0, 7.0])
        n = v1.normal(v2, v3)
        self.assertAlmostEqual(1.0, n.length)

    def test_normal_not_normalize(self):
        v1 = Vector3([2.0, 4.0, 2.0])
        v2 = Vector3([1.0, 7.0, 3.0])
        v3 = Vector3([8.0, 1.0, 7.0])
        n = v1.normal(v2, v3, False)
        self.assertNotAlmostEqual(1.0, n.length)


if __name__ == '__main__':
    unittest.main()
