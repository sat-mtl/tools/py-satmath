try:
    import unittest2 as unittest
except:
    import unittest
import numpy as np

from satmath.quaternion import Quaternion
from satmath.matrix44 import Matrix44
from satmath.matrix33 import Matrix33
from satmath.vector3 import Vector3
from satmath.vector4 import Vector4
from satmath.euler import Euler


class test_object_quaternion(unittest.TestCase):
    _shape = (4,)
    _size = np.multiply.reduce(_shape)

    def test_create(self):
        q = Quaternion()
        self.assertTrue(np.array_equal(q, [0., 0., 0., 1.]))
        self.assertEqual(q.shape, self._shape)

        q = Quaternion([1., 2., 3., 4.])
        self.assertTrue(np.array_equal(q, [1., 2., 3., 4.]))
        self.assertEqual(q.shape, self._shape)

        q = Quaternion(Quaternion([1., 2., 3., 4.]))
        self.assertTrue(np.array_equal(q, [1., 2., 3., 4.]))
        self.assertEqual(q.shape, self._shape)

    def test_from_x_rotation(self):
        # 180 degree turn around X axis
        q = Quaternion.from_x_rotation(np.pi)
        self.assertTrue(np.allclose(q, Quaternion([1., 0., 0., 0.])))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([1., 0., 0.])), [1., 0., 0.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 1., 0.])), [0., -1., 0.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 0., 1.])), [0., 0., -1.]))

        # 90 degree rotation around X axis
        q = Quaternion.from_x_rotation(np.pi / 2.)
        self.assertTrue(np.allclose(q, Quaternion([np.sqrt(0.5), 0., 0., np.sqrt(0.5)])))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([1., 0., 0.])), [1., 0., 0.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 1., 0.])), [0., 0., 1.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 0., 1.])), [0., -1., 0.]))

        # -90 degree rotation around X axis
        q = Quaternion.from_x_rotation(-np.pi / 2.)
        self.assertTrue(np.allclose(q, Quaternion([-np.sqrt(0.5), 0., 0., np.sqrt(0.5)])))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([1., 0., 0.])), [1., 0., 0.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 1., 0.])), [0., 0., -1.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 0., 1.])), [0., 1., 0.]))

    def test_from_y_rotation(self):
        # 180 degree turn around Y axis
        q = Quaternion.from_y_rotation(np.pi)
        self.assertTrue(np.allclose(q, Quaternion([0., 1., 0., 0.])))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([1., 0., 0.])), [-1., 0., 0.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 1., 0.])), [0., 1., 0.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 0., 1.])), [0., 0., -1.]))

        # 90 degree rotation around Y axis
        q = Quaternion.from_y_rotation(np.pi / 2.)
        self.assertTrue(np.allclose(q, Quaternion([0., np.sqrt(0.5), 0., np.sqrt(0.5)])))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([1., 0., 0.])), [0., 0., -1.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 1., 0.])), [0., 1., 0.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 0., 1.])), [1., 0., 0.]))

        # -90 degree rotation around Y axis
        q = Quaternion.from_y_rotation(-np.pi / 2.)
        self.assertTrue(np.allclose(q, Quaternion([0., -np.sqrt(0.5), 0., np.sqrt(0.5)])))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([1., 0., 0.])), [0., 0., 1.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 1., 0.])), [0., 1., 0.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 0., 1.])), [-1., 0., 0.]))

    def test_from_z_rotation(self):
        # 180 degree turn around Z axis
        q = Quaternion.from_z_rotation(np.pi)
        self.assertTrue(np.allclose(q, Quaternion([0., 0., 1., 0.])))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([1., 0., 0.])), [-1., 0., 0.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 1., 0.])), [0., -1., 0.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 0., 1.])), [0., 0., 1.]))

        # 90 degree rotation around Z axis
        q = Quaternion.from_z_rotation(np.pi / 2.)
        self.assertTrue(np.allclose(q, Quaternion([0., 0., np.sqrt(0.5), np.sqrt(0.5)])))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([1., 0., 0.])), [0., 1., 0.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 1., 0.])), [-1., 0., 0.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 0., 1.])), [0., 0., 1.]))

        # -90 degree rotation around Z axis
        q = Quaternion.from_z_rotation(-np.pi / 2.)
        self.assertTrue(np.allclose(q, Quaternion([0., 0., -np.sqrt(0.5), np.sqrt(0.5)])))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([1., 0., 0.])), [0., -1., 0.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 1., 0.])), [1., 0., 0.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 0., 1.])), [0., 0., 1.]))

    def test_from_axis_rotation(self):
        q = Quaternion.from_axis_rotation([1., 0., 0.], np.pi / 2.)
        self.assertTrue(np.allclose(q, Quaternion([np.sqrt(0.5), 0., 0., np.sqrt(0.5)])))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([1., 0., 0.])), [1., 0., 0.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 1., 0.])), [0., 0., 1.]))
        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 0., 1.])), [0., -1., 0.]))

    @unittest.skip('Not implemented')
    def test_from_euler(self):
        pass

    @unittest.skip('Not implemented')
    def test_from_inverse_of_euler(self):
        pass

    def test_length(self):
        q = Quaternion.from_x_rotation(np.pi / 2.0)
        self.assertAlmostEqual(q.length, 1.0)

    def test_normalize(self):
        q = Quaternion([1., 2., 3., 4.])
        self.assertFalse(np.allclose(q.length, 1.))

        q.normalize()
        self.assertTrue(np.allclose(q.length, 1.))

    def test_normalized(self):
        q1 = Quaternion([1., 2., 3., 4.])
        self.assertFalse(np.allclose(q1.length, 1.))

        q2 = q1.normalized
        self.assertFalse(np.allclose(q1.length, 1.))
        self.assertTrue(np.allclose(q2.length, 1.))

    def test_angle(self):
        q = Quaternion.from_x_rotation(np.pi / 2.0)
        self.assertEqual(q.angle, np.pi / 2.0)

    def test_axis(self):
        q = Quaternion.from_x_rotation(np.pi / 2.0)
        self.assertTrue(np.allclose(q.axis, Vector3([1.0, 0.0, 0.0])))

    def test_cross(self):
        q1 = Quaternion.from_x_rotation(np.pi / 2.0)
        q2 = Quaternion.from_y_rotation(np.pi / 2.0)
        self.assertTrue(np.allclose(q1.cross(q2), Quaternion([0.5, 0.5, 0.5, 0.5])))

    def test_dot(self):
        q1 = Quaternion.from_x_rotation(np.pi / 2.0)
        q2 = Quaternion.from_y_rotation(np.pi / 2.0)
        self.assertTrue(np.allclose(q1.dot(q2), 0.5))

    def test_conjugate(self):
        q = Quaternion.from_x_rotation(np.pi / 2.0)
        self.assertTrue(np.allclose(q.conjugate, Quaternion([-0.7071068, 0, 0, 0.7071068])))

    def test_inverse(self):
        q = Quaternion.from_x_rotation(np.pi / 2.0)
        self.assertTrue(np.allclose(q.inverse, Quaternion([-0.7071068, 0, 0, 0.7071068])))

    def test_power(self):
        q1 = Quaternion.from_x_rotation(np.pi / 2.0)
        self.assertTrue(np.allclose(q1.power(2.0), Quaternion([1.0, 0.0, 0.0, 0.0])))

    def test_negative(self):
        q = Quaternion.from_x_rotation(np.pi / 2.0)
        self.assertTrue(np.allclose(q.negative, [-0.70710678, 0., 0., -0.70710678]))

    def test_is_identity(self):
        self.assertTrue(Quaternion().is_identity)
        self.assertTrue(Quaternion([0., 0., 0., 1.]).is_identity)
        self.assertFalse(Quaternion([1., 0., 0., 0.]).is_identity)

    def test_matrix33(self):
        q = Quaternion.from_x_rotation(np.pi / 2.0)
        self.assertTrue(np.allclose(q.matrix33, Matrix33.from_quaternion(q)))

    def test_matrix44(self):
        q = Quaternion.from_x_rotation(np.pi / 2.0)
        self.assertTrue(np.allclose(q.matrix44, Matrix44.from_quaternion(q)))

    def test_operators_matrix33(self):
        q = Quaternion()
        m = Matrix33.from_x_rotation(0.5)

        # add
        self.assertRaises(ValueError, lambda: q + m)

        # subtract
        self.assertRaises(ValueError, lambda: q - m)

        # multiply
        self.assertTrue(np.array_equal(q.mul_matrix(m), Quaternion().cross(Quaternion.from_matrix(Matrix33.from_x_rotation(0.5)))))

        # divide
        self.assertRaises(ValueError, lambda: q / m)

    def test_operators_matrix44(self):
        q = Quaternion()
        m = Matrix44.from_x_rotation(0.5)

        # add
        self.assertRaises(ValueError, lambda: q + m)

        # subtract
        self.assertRaises(ValueError, lambda: q - m)

        # multiply
        self.assertTrue(np.array_equal(q.mul_matrix(m), Quaternion().cross(Quaternion.from_matrix(Matrix44.from_x_rotation(0.5)))))

        # divide
        self.assertRaises(ValueError, lambda: q / m)

    def test_operators_quaternion(self):
        q1 = Quaternion()
        q2 = Quaternion.from_x_rotation(0.5)

        # add
        self.assertRaises(ValueError, lambda: q1 + q2)

        # subtract
        # we had to add this to enable np.array_equal to work
        # as it uses subtraction
        # self.assertRaises(ValueError, lambda: q1 - q2)

        # multiply
        self.assertTrue(np.array_equal(q1 * q2, Quaternion().cross(Quaternion.from_x_rotation(0.5))))

        # divide
        self.assertTrue(np.array_equal(q1 / q2, q1 * q2.conjugate.normalized))

        # or
        self.assertTrue(np.array_equal(q1 | q2, Quaternion().dot(Quaternion.from_x_rotation(0.5))))

        # inverse
        self.assertTrue(np.array_equal(~q2, Quaternion.from_x_rotation(0.5).conjugate.normalized))

        # ==
        self.assertTrue(Quaternion() == Quaternion())
        self.assertFalse(Quaternion() == Quaternion([0., 0., 0., 0.]))

        # !=
        self.assertTrue(Quaternion() != Quaternion([1., 1., 1., 1.]))
        self.assertFalse(Quaternion() != Quaternion())

    def test_operators_vector3(self):
        q = Quaternion.from_x_rotation(0.5)
        v = Vector3([1., 0., 0.])

        # add
        self.assertRaises(ValueError, lambda: q + v)

        # subtract
        self.assertRaises(ValueError, lambda: q - v)

        # multiply
        self.assertTrue(np.allclose(q.mul_vector3(v), Vector3([1., 0., 0.])))

        # divide
        # self.assertRaises(ValueError, lambda: q / v)
        self.assertRaises(TypeError, lambda: q / v)

    def test_operators_vector4(self):
        q = Quaternion.from_x_rotation(0.5)
        v = Vector4([1., 0., 0., 1.])

        # add
        self.assertRaises(ValueError, lambda: q + v)

        # subtract
        # self.assertRaises(ValueError, lambda: q - v)

        # multiply
        self.assertTrue(np.allclose(q.mul_vector4(v), Vector4([1., 0., 0., 1.])))

        # divide
        # self.assertRaises(ValueError, lambda: q / v)
        self.assertRaises(TypeError, lambda: q / v)

    def test_apply_to_vector_non_unit(self):
        q = Quaternion.from_x_rotation(np.pi)

        # zero length
        v = Vector3([0., 0., 0.])
        self.assertTrue(np.allclose(q.mul_vector3(v), Vector3([0., 0., 0.])))

        # >1 length
        v = Vector3([2., 0., 0.])
        self.assertTrue(np.allclose(q.mul_vector3(v), Vector3([2., 0., 0.])))
        v = Vector3([0., 2., 0.])
        self.assertTrue(np.allclose(q.mul_vector3(v), Vector3([0., -2., 0.])))
        v = Vector3([0., 0., 2.])
        self.assertTrue(np.allclose(q.mul_vector3(v), Vector3([0., 0., -2.])))

    def test_accessors(self):
        q = Quaternion(np.arange(self._size))
        self.assertTrue(np.array_equal(q.xy, [0, 1]))
        self.assertTrue(np.array_equal(q.xyz, [0, 1, 2]))
        self.assertTrue(np.array_equal(q.xyzw, [0, 1, 2, 3]))

        self.assertTrue(np.array_equal(q.xz, [0, 2]))
        self.assertTrue(np.array_equal(q.xyz, [0, 1, 2]))
        self.assertTrue(np.array_equal(q.xyw, [0, 1, 3]))
        self.assertTrue(np.array_equal(q.xw, [0, 3]))

        self.assertEqual(q.x, 0)
        self.assertEqual(q.y, 1)
        self.assertEqual(q.z, 2)
        self.assertEqual(q.w, 3)

        q.x = 1
        self.assertEqual(q.x, 1)
        self.assertEqual(q[0], 1)
        q.x += 1
        self.assertEqual(q.x, 2)
        self.assertEqual(q[0], 2)

    def test_from_vector_track_up(self):
        direction = Vector3((0.0, 1.0, 0.0))
        q = Quaternion.from_vector_track_up(direction, 'Y', 'Z')
        self.assertTrue(np.array_equal(q.xyzw, [0.0, 0.0, 0.0, 1.0]))

        direction = Vector3((1.0, 0.0, 0.0))
        q = Quaternion.from_vector_track_up(direction, 'Y', 'Z')
        np.testing.assert_almost_equal(q.xyzw, [0.0, 0.0, -0.7071067690849304, 0.7071067690849304])


if __name__ == '__main__':
    unittest.main()
