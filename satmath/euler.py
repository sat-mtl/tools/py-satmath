import math
from numbers import Number
from typing import Any, List, Tuple, Union, Optional

import numpy as np  # type: ignore

from satmath.base import BaseMatrix, BaseRotation, BaseVector, EulerOrder, MathType, NpProxy


class Euler(BaseVector, BaseRotation):

    _shape = (3,)

    x = NpProxy(0)
    y = NpProxy(1)
    z = NpProxy(2)
    xy = NpProxy([0, 1])
    xz = NpProxy([0, 2])
    xyz = NpProxy([0, 1, 2])
    xzy = NpProxy([0, 2, 1])
    yzx = NpProxy([1, 2, 0])
    yxz = NpProxy([1, 0, 2])
    zxy = NpProxy([2, 0, 1])
    zyx = NpProxy([2, 1, 0])

    # TODO: Make configurable
    roll = NpProxy(0)
    pitch = NpProxy(1)
    yaw = NpProxy(2)

    @classmethod
    def identity(cls) -> 'Euler':
        return cls(np.zeros(cls._shape))

    @classmethod
    def from_x_rotation(cls, theta: float) -> 'Euler':
        return cls([theta, 0., 0.])

    @classmethod
    def from_y_rotation(cls, theta: float) -> 'Euler':
        return cls([0., theta, 0.])

    @classmethod
    def from_z_rotation(cls, theta: float) -> 'Euler':
        return cls([0., 0., theta])

    @classmethod
    def from_matrix(cls, matrix: 'BaseMatrix', order: EulerOrder = EulerOrder.XYZ) -> 'Euler':
        def clamp(x: float, mi: float, ma: float) -> float:
            return max(mi, min(ma, x))

        e = np.zeros(3)

        # assumes the upper 3x3 of m is a pure rotation matrix(i.e, unscaled)
        matrix = matrix.T
        m11 = matrix[0][0]
        m12 = matrix[0][1]
        m13 = matrix[0][2]
        m21 = matrix[1][0]
        m22 = matrix[1][1]
        m23 = matrix[1][2]
        m31 = matrix[2][0]
        m32 = matrix[2][1]
        m33 = matrix[2][2]

        if order == EulerOrder.XYZ:
            e[1] = math.asin(clamp(m13, -1.0, 1.0))
            if abs(m13) < 0.99999:
                e[0] = math.atan2(-m23, m33)
                e[2] = math.atan2(-m12, m11)
            else:
                e[0] = math.atan2(m32, m22)
                e[2] = 0.0

        elif order == EulerOrder.YXZ:
            e[0] = math.asin(- clamp(m23, -1.0, 1.0))
            if abs(m23) < 0.99999:
                e[1] = math.atan2(m13, m33)
                e[2] = math.atan2(m21, m22)
            else:
                e[1] = math.atan2(-m31, m11)
                e[2] = 0.0

        elif order == EulerOrder.ZXY:
            e[0] = math.asin(clamp(m32, -1.0, 1.0))
            if abs(m32) < 0.99999:
                e[1] = math.atan2(-m31, m33)
                e[2] = math.atan2(-m12, m22)
            else:
                e[1] = 0.0
                e[2] = math.atan2(m21, m11)

        elif order == EulerOrder.ZYX:
            e[1] = math.asin(- clamp(m31, -1.0, 1.0))
            if abs(m31) < 0.99999:
                e[0] = math.atan2(m32, m33)
                e[2] = math.atan2(m21, m11)
            else:
                e[0] = 0.0
                e[2] = math.atan2(-m12, m22)

        elif order == EulerOrder.YZX:
            e[2] = math.asin(clamp(m21, -1.0, 1.0))
            if abs(m21) < 0.99999:
                e[0] = math.atan2(-m23, m22)
                e[1] = math.atan2(-m31, m11)
            else:
                e[0] = 0.0
                e[1] = math.atan2(m13, m33)

        elif order == EulerOrder.XZY:
            e[2] = math.asin(- clamp(m12, -1.0, 1.0))
            if abs(m12) < 0.99999:
                e[0] = math.atan2(m32, m22)
                e[1] = math.atan2(m13, m11)
            else:
                e[0] = math.atan2(-m23, m33)
                e[1] = 0.0

        return cls(e, order=order)

    @classmethod
    def from_quaternion(cls, quaternion: 'Quaternion', order: EulerOrder = EulerOrder.XYZ) -> 'Euler':
        return cls.from_matrix(Matrix33.from_quaternion(quaternion), order)

    def __new__(
            cls,
            value: MathType = None, order: EulerOrder = EulerOrder.XYZ,
            roll: float = 0., pitch: float = 0., yaw: float = 0.,
            **kwargs: Any) -> 'Euler':
        if value is not None:
            obj = value
            if not isinstance(value, np.ndarray):
                obj = np.array(value)
                # TODO: Euler, if shape then pass
                # TODO: Matrix33, Matrix44
                # if obj.shape == (4, 4,) or obj.shape == (3, 3,) or isinstance(obj, (Matrix33, Matrix44)):
                #     obj = Euler.from_matrix(obj)
                # TODO: Quaternion
                # elif obj.shape == (4,) or isinstance(obj, Quaternion):
                #     obj = Euler.from_quaternion(obj)
        else:
            obj = np.array([roll, pitch, yaw])
        euler = super().__new__(cls, obj)
        euler.order = order
        return euler

    order = EulerOrder.XYZ

    def __floordiv__(self, other: Any) -> None:
        self._unsupported_type('floor divide', other)

    def __div__(self, other: Any) -> None:
        self._unsupported_type('divide', other)

    def __add__(self, other: Union['Euler', np.ndarray, List[float], Tuple[float, ...], Number]) -> 'Euler':
        return Euler(super().__add__(other))

    def __sub__(self, other: Union['Euler', np.ndarray, List[float], Tuple[float, ...], Number]) -> 'Euler':
        return Euler(super().__sub__(other))

    def __mul__(self, other: Union['Euler', np.ndarray, List[float], Tuple[float, ...], Number]) -> 'Euler':
        return Euler(super().__mul__(other))

    def __truediv__(self, other: Union['Euler', np.ndarray, List[float], Tuple[float, ...], Number]) -> 'Euler':
        return Euler(super().__truediv__(other))

    def __xor__(self, other: 'Euler') -> 'Euler':
        return self.cross(other)

    def __or__(self, other: 'Euler') -> float:
        return float(self.dot(other))

    def __ne__(self, other: 'Euler') -> bool:
        return bool(np.any(super().__ne__(other)))

    def __eq__(self, other: 'Euler') -> bool:
        return bool(np.all(super().__eq__(other)))

    @property
    def inverse(self) -> 'Euler':
        # for simplicity, we convert to a quaternion, get the inverse and convert back
        # to Euler (we could apply Rodrigues formula as well)
        q_inv = Quaternion.from_euler(self).inverse
        return Euler.from_quaternion(q_inv)

    ...


from satmath.matrix33 import Matrix33
from satmath.quaternion import Quaternion
