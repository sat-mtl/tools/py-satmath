from typing import Any, List, Tuple, Union
from numbers import Number
import numpy as np  # type: ignore

from satmath.base import BaseVector, MathType, NpProxy
from satmath.common import cross44


# TODO: add < <= > >= == != operators

class Vector4(BaseVector):

    # region Numpy

    _shape = (4,)

    x = NpProxy(0)
    y = NpProxy(1)
    z = NpProxy(2)
    w = NpProxy(3)
    xy = NpProxy([0, 1])
    xyz = NpProxy([0, 1, 2])
    xyzw = NpProxy(slice(0, 4))
    xz = NpProxy([0, 2])
    xw = NpProxy([0, 3])
    xyw = NpProxy([0, 1, 3])
    xzw = NpProxy([0, 2, 3])

    # endregion

    # region Creation

    @classmethod
    def from_vector3(cls, vector: Union['Vector3', np.ndarray], w: float = 0.0) -> 'Vector4':
        """
        Create a Vector4 from a Vector3.
        By default, the W value is 0.0.
        """
        return cls(np.array([vector[0], vector[1], vector[2], w]))

    @classmethod
    def from_matrix44_translation(cls, matrix: 'Matrix44') -> 'Vector4':
        return cls(np.array(matrix[3, :4]))

    @classmethod
    def x_axis(cls) -> 'Vector4':
        return cls([1.0, 0.0, 0.0, 0.0])

    @classmethod
    def y_axis(cls) -> 'Vector4':
        return cls([0.0, 1.0, 0.0, 0.0])

    @classmethod
    def z_axis(cls) -> 'Vector4':
        return cls([0.0, 0.0, 1.0, 0.0])

    @classmethod
    def w_axis(cls) -> 'Vector4':
        return cls([0.0, 0.0, 0.0, 1.0])

    def __new__(cls, value: MathType = None, x: float = 0.0, y: float = 0.0, z: float = 0.0, w: float = 0.0, **kwargs: Any) -> 'Vector4':
        if value is not None:
            if not isinstance(value, np.ndarray):
                obj = np.array(value)
            else:
                obj = value

            # # Vector4 (Most common, here for performance, otherwise we test all the others)
            # if obj.shape == (4,) or isinstance(obj, Vector4):
            #     pass
            #
            # # Vector3
            # elif obj.shape == (3,) or isinstance(obj, Vector3):
            #     obj = Vector4.from_vector3(obj)
            #
            # # Matrix44
            # elif obj.shape == (4, 4) or isinstance(obj, Matrix44):
            #     obj = Vector4.from_matrix44_translation(obj)
        else:
            obj = np.zeros(cls._shape)
        return super().__new__(cls, obj)

    # endregion

    # region Operators

    def __floordiv__(self, other: Any) -> None:
        self._unsupported_type('floor divide', other)

    def __div__(self, other: Any) -> None:
        self._unsupported_type('divide', other)

    def __xor__(self, other: Any) -> None:
        self._unsupported_type('XOR', other)

    def __add__(self, other: Union['Vector4', np.ndarray, List[float], Tuple[float, ...], Number]) -> 'Vector4':
        return Vector4(super().__add__(other))

    def __sub__(self, other: Union['Vector4', np.ndarray, List[float], Tuple[float, ...], Number]) -> 'Vector4':
        return Vector4(super().__sub__(other))

    def __mul__(self, other: Union['Vector4', np.ndarray, List[float], Tuple[float, ...], Number]) -> 'Vector4':
        return Vector4(super().__mul__(other))

    def __truediv__(self, other: Union['Vector4', np.ndarray, List[float], Tuple[float, ...], Number]) -> 'Vector4':
        return Vector4(super().__truediv__(other))

    # def __xor__(self, other):
    #    return self.cross(Vector4(other))

    def __or__(self, other: 'Vector4') -> float:
        return float(self.dot(Vector4(other)))

    def __ne__(self, other: 'Vector4') -> bool:
        return bool(np.any(super().__ne__(other)))

    def __eq__(self, other: 'Vector4') -> bool:
        return bool(np.all(super().__eq__(other)))

    # endregion

    # region Conversions

    @property
    def vector3(self) -> 'Vector3':
        return Vector3.from_vector4(self)

    @property
    def vector4(self) -> 'Vector4':
        return Vector4(self)

    def quaternion(self, track: str = 'Y', up: str = 'Z') -> 'Quaternion':
        return Quaternion.from_vector_track_up(Vector3.from_vector4(self), track, up)

    # endregion

    # region Methods and Properties

    def cross(self, other: 'Vector4') -> 'Vector4':
        return Vector4(cross44(self, other))

    @property
    def inverse(self) -> 'Vector4':
        return Vector4(-self)

    # endregion
    ...


from satmath.vector3 import Vector3
from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
