#!/bin/sh
{ mypy --incremental --strict --ignore-missing-imports satmath; } | awk '!seen[$0]++'
