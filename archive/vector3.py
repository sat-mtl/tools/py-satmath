"""
Provides functions for creating and manipulating 3D vectors.
"""

def generate_vertex_normals(vertices, index, normalize_result=True):
    """
    Generates a normal vector for each vertex.

    The result is a normalized vector.

    The index array should list the faces by indexing into the
    vertices array. It is assumed the ordering in index is
    counter-clockwise.

    The vertices and index arrays are Nd arrays and must be 2d,
    where the final axis is of size 3.

    An example::
        >>> vertices = numpy.array( [ [ 1.0, 0.0, 0.0 ], [ 0.0, 0.0, 0.0 ], [ 0.0, 1.0, 0.0 ] ] )
        >>> index = numpy.array( [ [ 0, 2, 1 ] ] )
        >>> vector.generate_vertex_normals( vertices, index )
        array([[ 0.,  0., 1.], [ 0.,  0., 1.], [ 0.,  0., 1.]])

    :param numpy.array vertices: an 2d array with the final dimension
        being size 3. (a vector)
    :param numpy.array index: an Nd array with the final dimension
        being size 3. (a vector)
    :param boolean normalize_result: Specifies if the result should
        be normalized before being returned.
    """
    v1, v2, v3 = np.rollaxis(vertices[index], axis=-2)
    face_normals = generate_normals(v1, v2, v3, normalize_result=False)
    vertex_normals = np.zeros_like(vertices)
    for i in range(3):
        np.add.at(vertex_normals, index[..., i], face_normals)
    if normalize_result:
        vertex_normals = normalize(vertex_normals)
    return vertex_normals
