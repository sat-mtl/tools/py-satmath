"""
3x3 Matrix which supports rotation, translation, scale and skew.

Matrices are laid out in row-major format and can be loaded directly
into OpenGL.
To convert to column-major format, transpose the array using the
numpy.array.T method.
"""

import numpy as np

def create_direction_scale(direction, scale):
    """
    Creates a matrix which can apply a directional scaling to a set of vectors.

    An example usage for this is to flatten a mesh against a
    single plane.

    :param numpy.array direction: a numpy.array of shape (3,) of the direction to scale.
    :param float scale: a float value for the scaling along the specified direction.
        A scale of 0.0 will flatten the vertices into a single plane with the direction being the
        plane's normal.
    :rtype: numpy.array
    :return: The scaling matrix.
    """
    """
    scaling is defined as:

    [p'][1 + (k - 1)n.x^2, (k - 1)n.x n.y^2, (k - 1)n.x n.z   ]
    S(n,k) = [q'][(k - 1)n.x n.y,   1 + (k - 1)n.y,   (k - 1)n.y n.z   ]
    [r'][(k - 1)n.x n.z,   (k - 1)n.y n.z,   1 + (k - 1)n.z^2 ]

    where:
    v' is the resulting vector after scaling
    v is the vector to scale
    n is the direction of the scaling
    n.x is the x component of n
    n.y is the y component of n
    n.z is the z component of n
    k is the scaling factor
    """
    if not np.isclose(np.linalg.norm(direction), 1.):
        direction = vector.normalize(direction)

    x, y, z = direction

    x2 = x ** 2.
    y2 = y ** 2.
    z2 = z ** 2

    scaleMinus1 = scale - 1.
    return np.array(
        [
            # m1
            [
                # m11 = 1 + (k - 1)n.x^2
                1. + scaleMinus1 * x2,
                # m12 = (k - 1)n.x n.y^2
                scaleMinus1 * x * y2,
                # m13 = (k - 1)n.x n.z
                scaleMinus1 * x * z
            ],
            # m2
            [
                # m21 = (k - 1)n.x n.y
                scaleMinus1 * x * y,
                # m22 = 1 + (k - 1)n.y
                1. + scaleMinus1 * y,
                # m23 = (k - 1)n.y n.z
                scaleMinus1 * y * z
            ],
            # m3
            [
                # m31 = (k - 1)n.x n.z
                scaleMinus1 * x * z,
                # m32 = (k - 1)n.y n.z
                scaleMinus1 * y * z,
                # m33 = 1 + (k - 1)n.z^2
                1. + scaleMinus1 * z2
            ]
        ]
    )
